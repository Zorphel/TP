# TP sauvegarde et FTP 

Le but du TP est : 

1. Mettre en place une architecture 

Host (fedora workstation 30 - localhost)

Guest1 (fedora server 30 - S1fedoralab - 192.168.100.10)

Guest2 (fedora server 30 - labserver  - 192.168.100.20) 

Outils :  *libvirt* , *virt-manager* et accès ssh sécurisé par clés (ed25519). 

2. Faire une image de la partition */dev/fedora/backup* au sein de la Guest2 (labserver 192.168.100.20) avec *dd* et la compresser.

3. Transférer de façon sécurisée (paire de clés ed25519) via *sftp* l'image sur la Guest1 (S1fedoralab 192.168.100.10).

4. Distribuer le fichier .xz en *ftp* public vers le Host via notre réseau isolé sur *virbr1* 192.168.100.1/24.

5. Sécuriser le serveur FTP avec certificats en TLS 1.3.

6. Vérifier l’intégrité de la solution de sauvegarde et mettre en place une routine.

L'intitulé de l’exercice ne précise pas si notre sauvegarde doit être incrémentielle ou pas . Cette notion ne sera pas abordée. 

Aucune notion de temps, de datation.

Aucune prérogative sur les outils à utiliser.

### Nous utiliserons les outils suivants:

* Outils : dd,lz4c,ssh,vsftpd


### Étape 1 mise en place des Guests au sein d'un réseau virtuel privé. 
  
  * Remarque : On part du principe que l’interface graphique fournie par *virt-manager* est on ne peut plus pratique. 
Afin de monter un laboratoire sur une workstation cette interface fait gagner beaucoup de temps. 
Il serait contre-productif voire suicidaire de se taper les commandes à la main dans avec *qemu* en cli.
Par ailleurs les configurations des Guests (fichiers .xml) sont facilement exportables. 
De cette façon on peut lancer nos Guests en production sur un serveur sans interface graphique avec ces précieux fichiers.
   
- __ A - Installation des outils de virtualisation sur le Host.  __


```console

# dnf install @virtualization
# gpasswd -a user libvirt
# gpasswd -a user kvm
# systemctl enable  --now libvirtd

```

L'installation des Fedora Server 30 sur les Guests est assez simple à effectuer grâce à l'interface graphique d'installation *anaconda*. Elle ne sera pas abordée.

Nous mettons en place un réseau virtuel privé dans les connexions de *libvirt*.

Le réseau virtuel permet de créer un environnement de travail cloisonné. Nous choisissons cette structure afin de distribuer nos services/fichiers pour le moment car il s'agit d'un laboratoire. 
Nous allons créer un réseau virtuel isolé qui sera bridgé sur l'interface *virbr1* . 192.168.100.1/24

Cette étape peut être réalisée en cli avec *visrh net-edit* ou en gui avec l'interface de *virt-manager*.

Bien entendu nous utilisons au préalable le NAT bridgé sur *virbr0* afin de mettre à jour les systèmes des Guests et d'installer les paquets nécessaires. 

- __ B - Création du réseau virtuel privé.  __

Dans le gui de *virt-manager* on crée le réseau virtuel c'est très facile. L'on peut également le fais en cli avec ; 

```shell 
visrsh net-edit isolated
```

```shell
<network>
  <name>isolated</name>
  <uuid>8f3b17dd-fae5-44cf-9786-d7e08c150d87</uuid>
  <bridge name='virbr1' stp='on' delay='0'/>
  <mac address='52:54:00:1c:45:5b'/>
  <domain name='isolated'/>
  <ip address='192.168.100.1' netmask='255.255.255.0'>
  </ip>
</network>
```
Mise en place du ssh sur les deux Guests: 

* Remarque : D'un point de vue de sysadmin l'on doit quasi systématiquement rendre disponible un serveur via une connexion ssh. 
 Nous utilisons *openssh-server* en le sécurisant par paire de clés *ed25519*. 

Cette étape est assez simple à mettre en place. 

Sur le Host : 

```
$ ssh-keygen -t ed25519 -f .ssh/id_S1_fedoralab
$ ssh-copy-id -i .ssh/id_S1_fedoralab
$ ssh -i .ssh/id_S1_fedoralab user@S1fedoralab
```


### Étape 2 et 3 en même temps. 

 * Remarque : Deux notions importantes entrent en considération dans la sauvegarde. 
Premièrement nous n'imageons pas un système en utilisation. 
En effet les modifications sur le système de fichiers monté et en cours d'utilisation tel un système (/) risque de nuire à l’intégrité de notre fichier image et il risque donc d’être corrompu ce qui annule le principe même de la sauvegarde.

Nous imagerons une partition qui n'est pas utilisée actuellement par un système. 
Dans cet exemple la partition */dev/fedora/backup*.

Secondement nous ne disposons pas sur le Guest *labserver* d'un espace nécessaire pour stocker le fichier image de notre partition. 
Nous le transférerons directement au travers du réseau en utilisant astucieusement dd, xz, ssh et le pipe.

- A - Utilisation des pipes.

Afin de réduire au maximum les commandes nous utilisons la merveilleuse chose qu'est le pipe. 

En effet avec deux pipes et les commandes qui vont bien l'on peut effectuer les étapes 2 et 3 en un seul coup. 
C'est un peu sale mais *dd* est un outil puissant qui permet de copier bit par bit une partition ou un disque. 

```shell
dd if=/dev/fedora/backup conv=notrunc status=progress | lz4c -9 | ssh -i .ssh/id_S1fedoralab root@S1_fedoralab dd of=/var/ftp/pub/labserver.xz

```

Nous précisons qu'aucunes règles dans le temps de la sauvegarde, ni de limite de bande passante ne sont précisées dans les consignes.

Si tel avait été le cas nous aurions pu limiter la bande passante avec *rsync --bwlimit=VALEUR* et ne pas utiliser d'outil de compression.

Nous prenons comme input dans dd notre partition en évitant les erreurs avec conv=notrunc.

Nous la compressons avec *lz4c -9* qui utilise le *lzma* niveau 9.

Nous nous connectons en *ssh* et redirigeons l'output de dd "of" vers un fichier .xz directement à la racine de notre serveur FTP Public.


### Étape 4 Serveur FTP public.

Sur la machine S1fedoralab : 

```console
# dnf install vsftpd
```

```console
# vim /etc/vsftpd.conf

# On active l(option FTP public : 
# Allow anonymous FTP? (Beware - allowed by default if you comment this out).
anonymous_enable=YES
```

On active le service *vsftpd* et on autorise le pare-feu à laisser transiter les paquets.

```console
# systemctl enable --now vsftpd
# firewall-cmd --permanent --add-service=ftp
```

Boom Shaka ! On peut accéder au backup ultra compressé en lzma sur le serveur ftp public.


### Étape 5 Sécurisation du serveur.  
