### TD realisé par : Jeanne Gilles Val.

@-- Git client-server organisation --@

Proposer des applications permettant une interaction client-serveur git agréable et efficace.

[ COTE CLIENT ]

Le client sera un service d'auto-push de la version locale du dépot distant.

Si l'utilisateur se connecte sur l'url du serveur sur un navigateur. Celui affichera une aborence complete du projet.
un jeu de couleur l'aidera a differencier qui a push quel fichier.

[ COTE SERVEUR ]

Deux serveurs:
-Un git
    il y aura plusieurs utilisateurs
-Un Http pour le index.html

Une application qui permettra de construire le index.html

On mettra en place en service de backup du dépot.
De facon périodique on archivera le dépot sur le serveur, nous permettant de pouvoir récupérer d'anciennes versions.
On supprimera celles jugées trop vielles.

-logs

[ A suivre ]

-je peux telecharger en cliquant sur les noms de fichiers. (+FTP)
-Programme de configuration des serveurs: frequence de sauvegarde, date limite, pouvoir modifier quand on veut le index.html...
-a vous de trouver

--- ---

## Presentation des outils et de la structure. 

HOST : FEDORA30
Virtualisation : Libvirt (kvm/qemu) ,virt-manager. 
Reseau virtuel sur virbr1 192.168.100.1/24
GUEST 1 : CENTOS STREAM S1GITPROJECT
GUEST 2 : CENTOS 7 S1BACKUP

--- 

## Sommaire

1. Installation du systeme CENTOS sur la VM1. 

2. Configuration du SSH entre le host et la VM1

3. Installation et configuration du serveur gitlab.

4. Creation des utilisateurs et verification du fonctionnement du serveur.

5. Installation de httpd sur la VM1 afin de distribuer l'index.html du code en couleurs.

6. 


# Installation du systeme sur la VM 1


Nous ne documentons pas l'installation de CENTOS sur la VM1 car l'interface graphique anaconda est tres simple à utiliser. 
Nous presentons le partitionnement et les premieres operations effectuées sur le systeme. 


```console
# lvs

  LV     VG              Attr       LSize   Pool   Origin Data%  Meta%  Move Log Cpy%Sync Convert
  pool00 cs_s1gitproject twi-aotz-- <29,98g               15,35  30,27                           
  root   cs_s1gitproject Vwi-aotz-- <19,98g pool00        19,58                                  
  swap   cs_s1gitproject -wi-ao----   1,60g                                                      
  var    cs_s1gitproject Vwi-aotz--  10,00g pool00        6,90      
  ```

Sur la VM1 : *S1GITPROJECT* nous verifions trois choses :

A - l'etat du pare-feu et l'application de la regle pour sshd
B - l'etat du serveur *sshd*, sa configuration et l'installation d'une paire de clés (ed25519).
C - l'etat des mises à jour et l'upgrade du systeme.


A - firewalld
```console
# systemctl status firewalld
# systemctl enable --now firewalld
```

Nous creeons une regle afin d'autoriser le traffic sur le port 22 pour le service sshd.
```console
# firewall-cmd --permanent --add-service=ssh
```

B - sshd

On verifie l'etat du service sshd

```console
# systemctl status sshd
# systemctl enable --now sshd
```

On configure le serveur pour accepeter les clés publiques (ed25519) et l'autentification par mot de passe dans un premier temps.
On edite */etc/ssh/sshd_config* :

```console
#	$OpenBSD: sshd_config,v 1.100 2016/08/15 12:32:04 naddy Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 22
AddressFamily any
ListenAddress 0.0.0.0
ListenAddress ::

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_dsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
SyslogFacility AUTHPRIV
#LogLevel INFO

# Authentication:

LoginGraceTime 2m
PermitRootLogin no
StrictModes yes
MaxAuthTries 6
MaxSessions 10

PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile	.ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
#PermitEmptyPasswords no
PasswordAuthentication yes

# Change to no to disable s/key passwords
#ChallengeResponseAuthentication yes
ChallengeResponseAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no
#KerberosUseKuserok yes

# GSSAPI options
GSSAPIAuthentication yes
GSSAPICleanupCredentials no
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no
#GSSAPIEnablek5users no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
# WARNING: 'UsePAM no' is not supported in Red Hat Enterprise Linux and may cause several
# problems.
UsePAM yes

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
X11Forwarding yes
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
#PrintMotd yes
#PrintLastLog yes
#TCPKeepAlive yes
#UseLogin no
#UsePrivilegeSeparation sandbox
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#ShowPatchLevel no
#UseDNS yes
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# Accept locale-related environment variables
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS

# override default of no subsystems
Subsystem	sftp	/usr/libexec/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#	X11Forwarding no
#	AllowTcpForwarding no
#	PermitTTY no
#	ForceCommand cvs server
```


Nous allons configurer la paires de clés afin d'acceer à nos VM à partir d'un terminal sur le Host.
Nous utilisons l'addresse IP NAT de la VM1 car nous avons des operations à faire necessitant l'acces à Internet.
Notez que nous avons crée un utilisateur user1 pendant l'installation du ssysteme.
Sur le Host :

```console
$ ssh-keygen -t ed25519 -f .ssh/S1_GITPROJECT
$ ssh-copy-id -i .ssh/S1_GITPROJECT.pub user1@192.168.4.115
$ ssh -i .ssh/S1_GITPROJECT user1@192.168.4.115
```

C - DNF UPDATE
 
```console
# dnf update
```

Notre systeme est à jour et pret à recevoir son serveur GIT. 
Afin de nous premunir d'eventuels problemes durant l'installation et la configuration nous effectuons un SNAPSHOT de notre VM1. 


Nous installons les outils necessaires et autorisons le traffic sur le port 80 (http) et 443 (https) .

```console
# yum install -y curl policycoreutils-python
# firewall-cmd --permanent --add-service=http
# firewall-cmd --permanent --add-service=https
# firewall-cmd --reload
```

Nous installons le service postfix qui gerera le serveur de mail. 

```console
# yum install postfix
# systemctl enable --now postfix
```

Cette commande permet de telecharger un script afin de modifier vos repositories .
Cette etape est importante mais doit etre effectuée avec parcimonie.
Par ailleurs n'executez jamais en root un script trouvé sur le net sans l'avoir au prealable lu et compris.

```console
# curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | bash
```

L'on remplace *https://gitlab.example.com* par l'addresse de notre serveur git. 

```console
# EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
```
